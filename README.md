# r_telecom

## Instalation
1) Make sure that you have used Python3 (3.6.7 prefered)
2) Install all required packages ```pip install -r requirements.txt```

## Parameters
You can change some usefull parameters in partial_reader.py.
1) path_to_data_file - file path to source data file
2) results_file_path - file path to save simple analysis result
3) test_chunksize - number of rows to read per iteration (it helps if you try to analysis big file)
4) top_destinations_count - count of top destinations addresses used 
5) percentile_bounds - list with percentiles used to make groups