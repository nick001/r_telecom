from collections import defaultdict

import pandas


def generate_chunks(file_path, sep='\t', chunksize=100):
    """
    Generator which read big csv file by rows, format data and return formatted
    dataframes one by one

    :param file_path: data source file path
    :param sep: csv file separator
    :param chunksize: number of rows to read (from big csv file), format (
        convert rtt value to millisecond and make colums names human readable)
        and return
    :type file_path: str
    :type sep: str
    :type chunksize: int
    """

    for dataframe in read_big_file(file_path, sep=sep, chunksize=chunksize):
        yield format_columns(dataframe)


def process_data(data, destinations_percentiles, n, boundaries):
    """
    Data processor. Get first n (same argument name) maximum requested
    destinations ip from data and add it to destination_percentiles (as
    DataFrames). Save only percentiles (as columns) and destination ip

    :param data: rtt dataframe used for calculations
    :param destinations_percentiles: dictionary used to store processed data
    :param n: count of top used destinations ip
    :param boundaries: percentile boundaries in (0, 1) interval
    :type data: pandas.DataFrame
    :type destinations_percentiles: dict
    :type n: int
    :type boundaries: list
    """

    # get first n top used destinations ip from data
    destinations_unique = data['destination_ip'].value_counts()
    top_used_destinations = destinations_unique[:n].index
    for destination_ip in top_used_destinations:
        # calculate rtt percentiles for destination ip
        destination_data = data.loc[data['destination_ip'] == destination_ip]
        percentiles = destination_data['rtt_in'].describe(boundaries)

        # convert percentiles to dataframe if new destination ip
        if destination_ip not in destinations_percentiles.keys():
            percentiles = pandas.DataFrame(percentiles)
        # add column to existing dataframe, using later for averaging
        else:
            percentiles = pandas.concat(
                [percentiles, destinations_percentiles[destination_ip]],
                axis=1
            )

        # add/update percentiles for destination ip by using current data chunk
        destinations_percentiles[destination_ip] = percentiles


def write_rtt_results(destinations_percentiles, percentagies, file_path):
    """
    Write destinations ip and rtt values to csv file. Find median value if have
    few data entries for destination ip.

    :param destinations_percentiles: preprocessed rtt data for different
        destinations ip
    :param percentagies: boundary values
    :param file_path: csv file path to save result
    :type destinations_percentiles: dict
    :type percentagies: list
    :type file_path: str
    """

    # find median percentile rtt values for each destination ip
    results = pandas.DataFrame()
    for destination_ip, percentiles in destinations_percentiles.items():
        results[destination_ip] = percentiles.median(axis=1)[percentagies]

    # calculate overall values
    results['median'] = results.median(axis=1)
    # percentiles in columns, destinations ips in rows
    results.T.to_csv(file_path)


def read_big_file(file_path, sep='\t', chunksize=100):
    """
    Method to read big csv file by rows

    :param file_path: path to big csv file with data
    :param sep: csv separator
    :param chunksize: number of rows to read each iteration
    :return: iterator of big file data content
    :type file_path: str
    :type sep: str
    :type chunksize: int
    :rtype: pandas.io.parsers.TextFileReader
    """

    return pandas.read_csv(
        file_path, sep=sep, chunksize=chunksize, header=None,
        usecols=[0, 1, 2, 3, 4], converters={4: convert_rtt_to_ms}
    )


def format_columns(dataframe):
    """
    Method to change columns names (column numbers to human readable values)
    and to change NaN values (id column) to 0. Returns formatted dataframe

    :param dataframe: 'raw' dataframe which need to be formatted
    :return: formatted dataframe
    :type dataframe: pandas.DataFrame
    :rtype: pandas.DataFrame
    """

    return dataframe.rename(
        columns={
            0: 'timestamp',
            1: 'source_ip',
            2: 'destination_ip',
            3: 'destination_port',
            4: 'rtt_in',
            5: 'id',
        }
    ).fillna(
        value={'id': 0}
    )


def convert_rtt_to_ms(rtt_value, default_val=0):
    """
    Convert rtt to milliseconds. To convert rtt to seconds you should divide
    value by 1500000000. Returns rtt value in milliseconds or 0

    :param rtt_value: rtt value from data source
    :param default_val: default rtt value to return it if something wrong
    :return: rtt value in milliseconds
    :type rtt_value: int
    :type default_val: int
    :rtype: int
    """

    try:
        return int(rtt_value) / 1500000
    except ValueError:
        return default_val


if __name__ == '__main__':
    # processing arguments
    path_to_data_file = 'napalabs/test_data_dpi.csv'
    results_file_path = 'rtt_results.csv'
    test_chunksize = 10000000
    top_destinations_count = 10000
    percentile_bounds = [5, 95]

    # process dataset
    percents = [percent / 100 for percent in percentile_bounds]
    rtt_results = defaultdict(pandas.DataFrame)
    for chunk in generate_chunks(path_to_data_file, chunksize=test_chunksize):
        process_data(chunk, rtt_results, top_destinations_count, percents)

    # save results
    percents = ['{}%'.format(percent) for percent in percentile_bounds]
    write_rtt_results(rtt_results, percents, results_file_path)
